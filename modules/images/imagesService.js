'use strict';

angular.module('lxdAdmin.containers')
    .factory('imagesService', ['$http', '$q', '$timeout','settings', 'authService',
        function ($http, $q, $timeout, settings, authService) {
            var obj = {};

            obj.isOperationFinished = function (operationUrl) {
                return $http.get(settings.apiUrl + authService.getContainer() + operationUrl + '/wait');
            };


            // Get all containers,  including detailed data
            obj.getAll = function () {
                // Sync
                return $http.get(settings.apiUrl + authService.getContainer() + settings.version + '/images').then(function (data) {
                    data = data.data;

                    if (data.status != "Success") {
                        return $q.reject("Error");
                    }

                    var promises = data.metadata.map(function (imagesUrl) {
                        return $http.get(settings.apiUrl + authService.getContainer() + imagesUrl).then(function (resp) {
                            return resp.data.metadata;
                        });
                    });

                    return $q.all(promises);
                });
            };

            return obj;
        }])
;
